// Andy Palmbach
// Assignment 2

#include <iostream> // need for cout
#include <conio.h> // need for getch

using namespace std;

enum Rank
{
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit
{
	Hearts,
	Diamonds,
	Clubs,
	Spades
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	Card c1;
	Card c2;
	Card c3;
	Card c4;
	Card c5;
	Card c6;
	Card c7;
	Card c8;
	Card c9;
	Card c10;
	Card c11;
	Card c12;
	Card c13;
	Card c14;
	Card c15;
	Card c16;
	Card c17;
	Card c18;
	Card c19;
	Card c20;
	Card c21;
	Card c22;
	Card c23;
	Card c24;
	Card c25;
	Card c26;
	Card c27;
	Card c28;
	Card c29;
	Card c30;
	Card c31;
	Card c32;
	Card c33;
	Card c34;
	Card c35;
	Card c36;
	Card c37;
	Card c38;
	Card c39;
	Card c40;
	Card c41;
	Card c42;
	Card c43;
	Card c44;
	Card c45;
	Card c46;
	Card c47;
	Card c48;
	Card c49;
	Card c50;
	Card c51;
	Card c52;

	c1.rank = Two;
	c1.suit = Hearts;

	c2.rank = Three;
	c2.suit = Hearts;

	c3.rank = Four;
	c3.suit = Hearts;

	c4.rank = Five;
	c4.suit = Hearts;

	c5.rank = Six;
	c5.suit = Hearts;

	c6.rank = Seven;
	c6.suit = Hearts;

	c7.rank = Eight;
	c7.suit = Hearts;

	c8.rank = Nine;
	c8.suit = Hearts;

	c9.rank = Ten;
	c9.suit = Hearts;

	c10.rank = Jack;
	c10.suit = Hearts;

	c11.rank = Queen;
	c11.suit = Hearts;

	c12.rank = King;
	c12.suit = Hearts;

	c13.rank = Ace;
	c13.suit = Hearts;

	c14.rank = Two;
	c14.suit = Diamonds;

	c15.rank = Three;
	c15.suit = Diamonds;

	c16.rank = Four;
	c16.suit = Diamonds;

	c17.rank = Five;
	c17.suit = Diamonds;

	c18.rank = Six;
	c18.suit = Diamonds;

	c19.rank = Seven;
	c19.suit = Diamonds;

	c20.rank = Eight;
	c20.suit = Diamonds;

	c21.rank = Nine;
	c21.suit = Diamonds;

	c22.rank = Ten;
	c22.suit = Diamonds;

	c23.rank = Jack;
	c23.suit = Diamonds;

	c24.rank = Queen;
	c24.suit = Diamonds;

	c25.rank = King;
	c25.suit = Diamonds;

	c26.rank = Ace;
	c26.suit = Diamonds;

	c27.rank = Two;
	c27.suit = Clubs;

	c28.rank = Three;
	c28.suit = Clubs;

	c29.rank = Four;
	c29.suit = Clubs;

	c30.rank = Five;
	c30.suit = Clubs;

	c31.rank = Six;
	c31.suit = Clubs;

	c32.rank = Seven;
	c32.suit = Clubs;

	c33.rank = Eight;
	c33.suit = Clubs;

	c34.rank = Nine;
	c34.suit = Clubs;

	c35.rank = Ten;
	c35.suit = Clubs;

	c36.rank = Jack;
	c36.suit = Clubs;

	c37.rank = Queen;
	c37.suit = Clubs;

	c38.rank = King;
	c38.suit = Clubs;

	c39.rank = Ace;
	c39.suit = Clubs;

	c40.rank = Two;
	c40.suit = Spades;

	c41.rank = Three;
	c41.suit = Spades;

	c42.rank = Four;
	c42.suit = Spades;

	c43.rank = Five;
	c43.suit = Spades;

	c44.rank = Six;
	c44.suit = Spades;

	c45.rank = Seven;
	c45.suit = Spades;

	c46.rank = Eight;
	c46.suit = Spades;

	c47.rank = Nine;
	c47.suit = Spades;

	c48.rank = Ten;
	c48.suit = Spades;

	c49.rank = Jack;
	c49.suit = Spades;

	c50.rank = Queen;
	c50.suit = Spades;

	c51.rank = King;
	c51.suit = Spades;

	c52.rank = Ace;
	c52.suit = Spades;

	_getch();
	return 0;
}